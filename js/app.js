function passVis(id, icon) {
    const element = document.getElementById(id);

    if (element.type === 'password') {
      element.type = 'text';
      icon.className = 'fas fa-eye-slash icon-password';
    } else {
      element.type = 'password';
      icon.className = 'fas fa-eye icon-password';
    }
}

function checkPass() {
    const pas = document.getElementById('pas').value;
    const pass = document.getElementById('pass').value;

    if (pas === pass) {
        alert('You are welcome');
    } else {
        const errorText = document.createElement('p');
        errorText.style.color = 'red';
        errorText.textContent = 'Потрібно ввести однакові значення';
        document.querySelector('.password-form').appendChild(errorText);
    }
}